"use strict";

const request = require("request");
const express = require("express");

const port = 3055;
const apiKey = "f24feb6ef3f009c7fdbf71e42e27bc584c4507975a27a52523c3d58a2916ec76";

const app = express();
app.use(express.json());

app.post('/', (req, res) => {
    console.log(req.body);
    const values = req.body.document;
    request
        .post(`https://search.in-two.com/collect/api/marconi/insert?key=${apiKey}`, {
            json: {
                "message_id": values._id,
                "user_id": values._to,
                "text": values.body,
                "timestamp": new Date(values.dateTime).getTime()
            }
        })
        .on('response', (response) => {
            console.log(response.statusCode, response.statusMessage);
            res.status(response.statusCode).end(response.statusMessage);
        })
        .on('error', (error) => res.status(422).end(error));
});

// app.post('/delete/all', (req, res) => {
//     request
//         .post(`https://search.in-two.com/collect/api/marconi/erase/all?key=${apiKey}`)
//         .on('response', (response) => {
//             console.log(response.statusCode, response.statusMessage);
//             res.status(response.statusCode).end(response.statusMessage);
//         })
//         .on('error', (error) => res.status(422).end(error));
// });

app.listen(port, () => {
    console.log('Listening to:', "localhost:" + port);
});